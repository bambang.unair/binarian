//--------LATIHAN 1 Chapter 5-Express------

const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  const body = `
      <h2>Landing Page</h2>
      <a href="http://localhost:4000/catalog">Catalog</a> |
      <a href="http://localhost:4000/contact">Contact Us</a> |
      <a href="http://localhost:4000/page-not-found">Another Route</a>
      `;

  res.send(body);
});
app.get("/catalog", (req, res) => {
  res.send("<h1>Catalog</h1>");
});
app.get("/contact", (req, res) => {
  res.send("<h1>Contact Us</h1>");
});
app.get("/page-not-found", (req, res) => {
  res.send("<h1>Another Route</h1>");
});
app.use("/", (req, res) => {
  res.status(404);
  res.send("<h1>404</h1>");
});

// app.listen(port, () => console.log(`example app listening at http://${port}`));

//--------LATIHAN 2 Chapter 5-Express------

// const express = require("express");
// const app = express();
// const port = 3000;

const logger = (req, res, next) => {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1;
  let yyyy = today.getFullYear();
  let hh = today.getHours();
  let nn = today.getMinutes();
  let ss = today.getSeconds();

  if (dd < 10) dd = "0" + dd;
  if (mm < 10) mm = "0" + mm;
  if (hh < 10) hh = "0" + hh;
  if (nn < 10) mm = "0" + nn;
  if (ss < 10) ss = "0" + ss;

  let date = dd + "/" + mm + "/" + yyyy;
  let time = hh + ":" + nn + ":" + ss;

  console.log("Request GET/ at " + date + "" + time);
  next();
};
app.use(logger);
// // //    * Using middleware
// // //    * Endpoint untuk landing page => Using Middleware (header)
app.listen(port, () => console.log(`example app listening at http://${port}`));
